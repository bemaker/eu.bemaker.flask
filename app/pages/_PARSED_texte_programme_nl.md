title: Programme
published: 2018-08-23
contenu_statique: oui
nl: oui
folderImg: .

# NDLS

Notre programme combine une découverte du tech truck et d’un atelier à l’école, des tutoriels en ligne et des ateliers de suivi via un réseau de fablab et autres acteurs de formation au numérique.

### Tech-truck à l’école

Notre activité de sensibilisation se compose de la **visite du tech-truck** et d'un
**atelier participatif** en classe sur une durée totale de 2H30 (trois périodes de 50 minutes). Seuls une place de parking, une prise électrique à moins de 40 mètres et un local sont nécessaires à notre visite.

Dans un premier temps, les élèves visitent le Tech-Truck. Les machines y sont installées et leurs fonctionnements expliqués. L'imprimante 3D, similaire au pistolet à colle ? Des coordonnées X Y Z, comme au cours de math,  pour la commander ? Rien de sorcier dans tout ça  !

Il s'agit avant tout de **démystifier** les technologies de création mais aussi d'**inspirer** le jeune en lui montrant différent projet et ainsi libérer son imagination et sa créativité .

Ensuite, les élèves retournent en classe pour un atelier participatif où ils pourront eux même tester activement ces nouvelles technologies.

### Tuto, quizz et projet en ligne

Via notre **plateforme web**, nous proposons aux jeunes de continuer leur parcours makers. Des tutoriels en ligne leur permettront d'approfondir les techniques appréhendées lors de la visite du tech truck(impression 3D, laser, CNC, etc.) et des quizzs challegeront leur apprentissage.
Grâce à des exemples de projets à réaliser, le maker en herbe peut également se lancer dans la réalisation de prototype concret.


### Le réseau maker en Belgique
Finalement, nous proposons également aux jeunes de s’inscrire à des **ateliers et événements makers** accessibles à proximité de chez lui.

Ce suivi est rendu possible grâce à la mise en réseau de nombreuses associations travaillant dans le secteur numérique et de la jeunesse. Découvre-les ici.
