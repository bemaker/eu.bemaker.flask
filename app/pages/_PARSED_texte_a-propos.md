title: A propos
published: 2018-08-23
contenu_statique: oui
fr: oui
folderImg: .

BeMaker souhaite **inspirer et former les jeunes à la création par les nouvelles technologies**. Équipé des machines issues des Fablabs (Imprimante 3D, Laser, CNC, etc.), notre tech-truck se rend gratuitement dans les écoles. Grâce à la découverte de ces technologies et de leurs capacités de création presque infinies, nous souhaitons **stimuler la créativité des jeunes et les ouvrir à un nouveau monde des possibles**.

Entièrement financé, notre programme est **gratuit et accessible à toutes les écoles**. Nous oeuvrons  pour un système scolaire équitable où tous les jeunes peuvent se former aux nouvelles technologies et se préparer aux enjeux sociétaux de demain.

BeMaker participe ainsi à une **intégration du numérique à l'école**; une intégration réfléchie, critique et surtout créative. Les technologies présentes dans notre fablab mobile sont mises au service de la créativité pour inventer et concevoir des objets tangibles du quotidien. Un lien entre le monde numérique et le monde réel permettant aux jeunes de passer de consommateur de technologie à créateur.
