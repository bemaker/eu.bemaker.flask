title: Impression 3D
published: 2010-12-24
featured_image:
  - img/photos/IMG_3576_crop.JPG
  - img/photos/IMG_3577_crop.JPG
  - img/photos/IMG_3588_crop.JPG
  - img/photos/IMG_3596.jpg
  - img/photos/IMG_3598.jpg
quiz: impression3d
badge: 3dprint
cat: usinage
folderImg : 3dprint
desc: introduction à l'impression 3d

[TOC]

# L'impression 3D

Imprimer en 3D ? Facile!

Aujourd’hui, l’imprimante 3D, abordable et facile d’utilisation,  est à la portée de tous. Ces possibilités de création sont infinies : décoration, jeux, pièces de remplacement, etc.
Ce tutoriel te permettra de comprendre le fonctionnement de l’imprimante 3D, ses possibilités d’utilisation et les bases techniques pour tenter toi même l’expérience.

Découvre les bases de l’impression 3D et gagne ton badge pour en devenir un expert!

## Principe et fonctionnement

### Abracadabra, objet imprime-toi!
Actuellement, l’impression 3D est sur toutes les lèvres. Commercialisée au grand public, cette technique de création est de plus en plus populaire. Pourtant, les imprimantes 3D existent depuis le milieu des années 1980[^chuckhull].

Magique l’impression 3D? Au contraire, son procédé de fabrication est assez simple.

L’impression 3D permet de fabriquer des objets ou des pièces en volume selon un procédé de **fabrication additive**. Toutes les formes d’impression 3D reposent sur le même principe:  l’ajout de matière couche après couche pour créer du volume.

Imaginez : vous prenez une feuille blanche dans laquelle vous découpez un carré. Vous obtenez une forme en deux dimensions. Maintenant, répétez l’opération un grand nombre de fois et superposez les carrés. Après un moment, le carré se transforme en cube.  L’impression 3D  fait exactement la même chose et permet  donc de créer du volume grâce à superposition de la matière première.

### Différentes formes d’impression
Il existe principalement trois formes d’impression reposant cependant tous sur le même principe : l'ajout de matière couche après couche pour créer​ du volume.

- FDM : Fusion Deposit Material -Dépôt de matière en fusion

La méthode d’impression la plus répandue se nomme “FDM”- Fusion Deposit Material- Dépôt de matériel en fusion.

La popularité de cette technique est due à sa facilité d’utilisation. La matière première est le filament plastique. Ce dernier est  poussé à travers la tête d’impression qui le fait fondre. La température de fusion se situe généralement entre 95 et 260 °C. Une fois fondu, le plastique se dépose couche par couche sur le lit d’impression. Les couches fusionnent les unes avec les autres.

<iframe width="560" height="315" src="https://www.youtube.com/embed/WHO6G67GJbM" frameborder="0" allowfullscreen></iframe>

- SLA : la stereolithographie

La forme la plus répandue de Stereolithography est la Photopolymérisation. Dans ce procédé, un photopolymère (résine réactive à la lumière, le plus souvent UV)  est durci  grâce à un laser ultra-violet.

L’opération est reproduite couche par couche. Elles se superposent  ensuite pour créer l’objet.

<iframe width="560" height="315" src="https://www.youtube.com/embed/NM55ct5KwiI" frameborder="0" allowfullscreen></iframe>

- SLS : forgeage par application laser

Dérivé de la stéréolithographie, le forgeage par application laser crée des objets 3D en superposant des couches durcie  via l’utilisation d’un laser.
Cette technique permet de travailler avec des matières premières telles que la céramique ou l’argent.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0sLcobtfHFY" frameborder="0" allowfullscreen></iframe>

## A quoi cela sert-il?   

Décoration, bijoux, pièce de jeux, l’impression 3D peut servir à imprimer de nombreux objets de notre quotidien mais elle est également largement répandue dans le secteur industriel. En effet, elle offre de nombreuses opportunités.  Elle permet d’imprimer rapidement des prototypes ou encore de développer des chaînes de production courte.

Certains secteurs l’utilisent tous les jours! En voici quelques exemples:

- Le secteur médical

L’impression 3D offre des grandes opportunités dans le secteur médical. Elle permet notamment d’imprimer des prothèses ou des implants personnalisés à faible coût.

Le projet [Open Hands](http://www.openhandproject.org/), par exemple, regroupe une communauté de bénévoles travaillant sur des prothèses de la main. Basé sur l’open source, les fichiers sont accessibles gratuitement.


- L'aéronautique

Le secteur aéronautique utilise les techniques d’impression en volume pour produire des pièces plus rapidement et à coût réduit[^aéronotique].

- L’aérospatial

La NASA utilise une imprimante 3D pour fabriquer des pièces dans leur station spatiale internationale.

Aujourd’hui, l’imprimante 3D se démocratise et cette nouvelle technologie peut rentrer dans notre quotidien. Une opportunité offrant de nombreuses possibilités!


## De l’idée à l’objet en trois étapes

Tout comme pour une impression normale, avant de lancer l’imprimante, il faut avoir un fichier à imprimer. Et dans ce cas-ci, un fichier en volume avec trois dimensions. Ces fichiers existent le plus souvent dans le format **stl ou obj**.

Dans le processus d’impression, on distingue trois étapes :
- Obtenir un fichier
- Préparer le fichier
- Imprimer le fichier


### Étape 1: Obtenir un fichier 3D

Il existe diverses manières pour obtenir un fichier en volume. Les plus simples sont :
- la modélisation 3D
- le téléchargement d’un fichier                   

**Modélisation**

Créer un fichier d’impression 3D requiert l’utilisation d’un logiciel de modélisation 3D.

En voici quelques exemples :
- [blender](https://www.blender.org/) opensource / gratuit / multi-OS
- [freeCad](https://www.freecadweb.org/) opensource / gratuit / multi-OS / paramatric
- [opensCad](http://www.openscad.org/) opensource / gratuit / multi-OS / paramatric / no-GUI
- [fusion360](http://www.autodesk.com/products/fusion-360/overview)  close source /windows-osx / gratuit[^free] / parametric  

**Téléchargement d'un fichier**

Il existe plusieurs plateformes de téléchargement de fichier 3D prêt pour l’impression. On y trouve des objets à créer (coque de GSM, bijoux, figurine, etc.) ou bien des pièces pour réparer des ustensiles du quotidien.

Les deux sites principaux sont :
- [http://www.thingiverse.com/](http://www.thingiverse.com/) grande diversité de fichiers classés par thème (site soutenu par *Makerbot* fabricant des machines *replicatorBot* )
- [https://www.youmagine.com/](https://www.youmagine.com/) (site soutenu par *Ultimaker* fabricant des machines *ultimaker* )

### Étape 2: la préparation des fichiers
Pour imprimer, il faut préparer le fichier afin qu’il soit correctement interprété par la machine.

Rappelons le principe de base de l’impression : des couches superposées les unes aux autres. L’objet 3D doit donc être “découpé” en couches pour être imprimé par la machine.

Des logiciels spécialisés sont réaliser cette “découpe”. Par exemple:
- CURA (ultimaker)
- Slic3r
- Repetier

Différents options doivent être paramétrées pour notre impression:
- La hauteur des tranches ( En général entre 0.1 et 0.5mm en fonction de la qualité voulue)
- La température d’impression pour le plastique sélectionné
- Le besoin de support de maintien de l’impression
- La quantité de remplissage de l’objet


Une fois réglé, le programme exporte notre objet dans un langage de programmation **Gcode** lisible par l’imprimante.
Le Gcode est un langage de programmation utilisé par les machines de création technique (Imprimante 3D, CNC, laser, etc.). Ce code est composée de “commandes” qui permet à l’imprimante d’identifier les mouvements à exécuter via l’utilisation de “coordonnées” XYZ qui lui indiquent où se positionner.  
`g01 x100 y200 z0.3 e50`


### Étape 3:  Impression!
Pour un accès facile à une imprimante 3D, n’hésite pas à te rendre dans un fablab près de chez toi.  Tu trouvera ici une [liste de quelques fablabs partenaires](http://www.bemaker.eu/program) qui pourront t’aider.




[^chuckhull]:L'imprimante 3D a été créé par Chuck Hull. Il a inventé   la stéréolithographie. Biographie de Chuck Hull - [link](https://en.wikipedia.org/wiki/Chuck_Hull)
[^aéronotique]: Pour plus d'informations sur l'utilisation de l'imprimante 3D en aéronotique-[link](https://www.cnbc.com/2017/04/11/boeing-looks-to-trim-costs-with-first-ever-3d-printed-plane-structure.html)
[^free]: gratuit pour etudiants, hobbyiste et non commercial avec inscription et quelques limitation
[^gcode]: plus d'informations sur le gcode - [link](http://reprap.org/wiki/G-code)
