title: weather station
published: 2010-12-22
desc: petite station meteo
cat: usinage
folderImg : wtrstn

#STATION METEO / introduction  à ARDUINO

![](http://localhost:5000/static/page_images/wtrstn/ws_board.jpg)

## les enjeux
- découvrir un language de programmation ( arduino / c )
- les connections électroniques
- les capteurs
- les signaux analogues et digitaux

>  programme sur 4 sessions de 2h



### MATERIEL

| item                   |  nbr  |
| :--------------------- | :---: |
| arduino uno            |   1   |
| capteur hum/temp dht22 |   1   |
| ecran Oled1.3 en i2c   |   1   |
| photo resistance       |   1   |
| jumper wire            | 1 kit |


## jour 1 : introduction et familiarisation
- introduction pour beMaker
- discussion sur l'openSource openHarware
- premier contact avec l’environement arduino

#### Bemaker / FablabMobile
- qui sommes-nous
- la volonté derrière la création de l'asbl
- les fablabs et leurs atouts (machines  / communauté )

#### OpenSource / OpenHardware
- accessibilité des informations / savoir
- le champs des possibles de la création à coût réduit avec aide et possibilité de modifier

### Arduino premier contact

#### Arduino IDE et son interface

interface de base de Arduino <abbr title='Integrated development environment' >IDE</abbr> [*](https://en.wikipedia.org/wiki/Integrated_development_environment)

![](http://localhost:5000/static/page_images/wtrstn/ide001_.png)


##### les menus importants

== avant toute chose bien vérifier que la carte *arduino* est la bonne et que le port sélectionné correspond dans le menu  `TOOLS`==


 ![](http://localhost:5000/static/page_images/wtrstn/ide2_.PNG)


#####  premier code

ouverture d'un exemple simple `blink`

`File > Exemple > 01.basic > blink`

![](http://localhost:5000/static/page_images/wtrstn/blink.png)


```c
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

faire un upload sur l' *arduino*  ( <kbd>CTRL</kbd>+ <kbd>U</kbd> )

>**Exercice**
faire varier le temps de clignotement , 2x plus rapide ? 2 fois plus ***on*** que ***off***


##### introduction à l’idée de variable

- qu'est-ce qu'une variable ? [en savoir plus ](https://fr.wikipedia.org/wiki/Variable_(informatique))


> **Exercice**
créer une variable pour le temps de `delay` afin de pouvoir faire des modifications facilement

*solution*
```c

int delayVal = 1000 ; 				 // creation de la variable

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(delayVal);  				// utilisation de la variable
  digitalWrite(LED_BUILTIN, LOW);
  delay(delayVal);					// utilisation de la variable
}
```

**bonus**
une led 2 fois plus `off` que `on`
```c

int delayVal = 1000 ; 				 // creation de la variable

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(delayVal);  				// utilisation de la variable
 digitalWrite(LED_BUILTIN, LOW);
 delay(delayVal*2);					// utilisation de la variable x 2
}
```



##### Utilisation d'une variable pour gérer une information

faire les branchements sur la carte *arduino* ==UNIQUEMENT avec une carte débranchée==

prendre un `potentiometre` et faire les branchements comme ceci :

![](http://localhost:5000/static/page_images/wtrstn/pot_bb.png)  

le code

```c

const int potPin = A0 ; 				 // Création de la variable pour le pin input du potentiometre sur le port analogique 0
int potVal  = 0 ;					// innitialisation de la variable pour recevoir la valeur lu sur le port A0
int ledPin = 13;					// Création de la variable pour le pin output pour une led (sur le port 13 en premier car protégé par une resistance)

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
	potVal = analogRead(potPin);
 	digitalWrite(ledPin, HIGH);
	delay(potval);
	digitalWrite(ledPin, LOW);
	delay(potVal);
}
```

**Details**

`const int potPin = A0 ; `

- **const** : déclaration d’une variable constante (le port A0 ne va pas changer)
- **int** : création d'une variable de type `integer` , nombre entier
- **potPin** = A0 : le potentiometre sera donc branche sur le port A0 de l' *arduino*

 `int potVal  = 0 ;`

- **int** : création d'une variable de type `integer` , nombre entier
- **potVal**  = 0 : innitialisation de la valeur a `0` la valeur sera mise a jour par la suite

`int ledPin = 13;`

- **int** : création d'une variable de type `integer` , nombre entier
- **ledPin** = 13 : la led sera donc branchée sur le port 13 de l' *arduino*

`void setup()`
- partie du programme exécuté UNE SEULE FOIS au démarrage de la carte *arduino*

`pinMode(ledPin, OUTPUT);`
- déclaration de la Led en tant que OUTPUT (sortie)

`void loop() `
- partie du programme qui sera répété en boucle encore et encore tant que la carte reste allumée


```c
	potVal = analogRead(potPin);		 // lecture de la valeur sur le port A0 de larduino et stockage de la valeur (0-1023) dans la variable prévue
 	digitalWrite(ledPin, HIGH); 		// mise sous tension de la led (on allume)
	delay(potval); 					// faire une pause de la durée récupère par le capteur (potentiometre)
	digitalWrite(ledPin, LOW);		// mise hors tension de la led (on éteint)
	delay(potVal);					// faire une pause de la durée récupérée par le capteur (potentiometre)

```

*Bravo fin du jour 1!*


## jour 2 : Premier code avec une librairie

**au programme**

- <abbr title='pulse with modulation'> P.W.M </abbr>
- la conversion de donnée (fonction `map()` )
- les différences entre signaux (logic / pwm /analogique)

### mise en place

 ouverture d'un exemple

`File > Exemples > 01.basics > fade`

---
```

int led = 9;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // set the brightness of pin 9:
  analogWrite(led, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}

```

> **exercice**:
faire en sorte que la lumière ne s’éteigne jamais malgré les variations

explication du pwm
: le pwm est la façon électronique de reproduire un signal analogique (par opposition au signal logic `on - off `)
le découpage pour la variation

### experimentation

le but est maintenant de faire un programme de type *dimmer*, pouvoir contrôler l’intensité de la lumière avec le *potentiometre*

check list
- un potentiometre sur le pin `A0` de larduino
- une led  sur un port `pwm` de l'arduino (pin avec `~`) et une résistance adapte (10k&#8486;)

####goal
la valeur du potentiometre pour faire varier la luminosité de la led

**code**

todo

- déclaration des variables utiles
- mise en place du setup pour les entrées / sorties
- lecture et applications des variables sur la led et le pwm


**branchement**

 ![](http://localhost:5000/static/page_images/wtrstn/pot_bb.png)  


**code**


```c
const int potPin = A0 ; 	//declaration de l'entre analogique du potentiometre
const int ledPin = 13; 	// declaration de la sortie pour la led

int potVal = 0 ; 		//creation de la variable pour la valeur de potentiometre
int pwmOutput = 0 ; 	//creation de la variable pour le valeur de sortie pour la led

void setup (){
	pinMode(ledPin, OUTPUT);	 //declaration de la led en tant que sortie de larduino
}

void loop (){
	pwmOutput = analogRead(potPin); 			//lecture de la valeur sur l'entre analogique
	pwmOutput = map (pwmOutput , 0,1023,0,255);	// conversion de la valeur pour le p.w.m
	analogWrite(ledPin,pwmOutput);				// application de la valeur sur le port de sortie de la led
	delay(100);
}
```
>il est inutile de déclarer le port `A0` comme entrée car les ports analogique ne sont que des entrées  

**Explication et théorie**

la function `map()`
>pourquoi la fonction `map()` ; l'*arduino* ne peut pas faire de signal analogique à proprement parler, il ne peut que être `HIGH` ou `LOW `, 0 ou 1 , 0V ou 5v pas 2,5v ou 3v pour compenser cette impossibilité les micros controleurs peuvent faire du <abbr title='pulse with modulation'> P.W.M </abbr> [en savoir plus >](https://fr.wikipedia.org/wiki/Modulation_de_largeur_d%27impulsion)  c’est à dire allumer ou éteindre rapidement un pin de sortie à différentes fréquences pour que la tension moyenne de sortie corresponde à une valeur d'entre deux .
![](http://localhost:5000/static/page_images/wtrstn/pwm.gif)
~source Wikipedia~
Pour le microcontroleur de l' *arduino* il y a 255 valeurs possibles (micro controleur *8bits*  [`11111111`] 128 + 64 + 32 + 16 + 8 +4 + 2 + 1 = 255 ).
le signal analogique de lecture lui en a 1024 [0,1023] la fonction `map()` va nous permettre de faire le calcul et de *remapper* ces valeurs
0 -> 0
512 -> 127
1023 -> 255
c'est un produit en croix qui s’opère dans la fonction ce qui nous permet d'exploiter l’ensemble de la course de notre protentiometre.

~**nb** il sera utile de comprendre la fonction map dans le cadre de la construction du bras robotique pour notre servo moteur `0-1024` -> `0-180`~

### premier mouvement

#### au programme

- première librairie externe
- branchement d'un servo moteur
- contrôle de l'angle avec le potentiometre


**Test du servo moteur**

Ouvrir l'exemple pour le servo

 <kbd>file</kbd> > <kbd> exemples</kbd> > <kbd>librarie</kbd> >  <kbd>sweep</kbd>

une fois l'exemple ouvert vous devriez avoir ce code

```c
 #include <Servo.h>

Servo myservo;  // create servo object to control a servo
		// a maximum of eight servo objects can be created

int pos = 0;    // variable to store the servo position

void setup()
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}
void loop()
	{
	  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees
	  {                                  // in steps of 1 degree
	    myservo.write(pos);              // tell servo to go to position in variable 'pos'
	    delay(15);                       // waits 15ms for the servo to reach the position
	  }
	  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees
	  {
	    myservo.write(pos);              // tell servo to go to position in variable 'pos'
	    delay(15);                       // waits 15ms for the servo to reach the position
	  }
	}
```

Info
> un servo est un moteur un peu spécial; nous pouvons lui demander un angle précis entre 0 et 180 deg (il ne tourne pas sur lui même mais se déplace sur un demi cercle, pour la plupart d’entre eux)
![servo](http://localhost:5000/static/page_images/wtrstn/servomotor.jpg  "interieur servo")
le servo utilise un potentiometre pour connaître sa position


maintenant contrôlons le servo nous même

 <kbd>file</kbd> > <kbd> exemples</kbd> > <kbd>librarie</kbd> >  <kbd>knob</kbd>

 le code suivant est chargé

~~~c
 ```
#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int potpin = 0;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin

void setup()
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop()
{
  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 179);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  delay(15);                           // waits for the servo to get there
}
 ```
~~~

#### Explication du code

	#include <Servo.h>

Chargement de la librairie `servo`
une *librairie* est un dictionnaire de fonction qui va nous permettre d’augmenter les fonctions d'arduino simplement (sans avoir à écrire bcp de code la librairie va nous permettre de gérer pleins de choses facilement)

	Servo myservo

déclaration d'un *objet* 	`Servo` qui va nous permettre par la suite d’interagir avec ce servo via la variable `myservo`

```c
	int potpin = 0;  // analog pin used to connect the potentiometer
	int val;    // variable to read the value from the analog pin
```

déclaration des variables utiles pour la suite de notre code



```c
	void setup()
		{
 	 	myservo.attach(9);  // attaches the servo on pin 9 to the servo object
		}

```

Dans le setup; utilisation d'un outil de la librairie pour associer le servo au port de sortie numéro `9` de l'arduino pour ainsi plus facilement y avoir acces par la suite


```c
 val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 179);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  delay(15);
```

dans l’ordre

1. on récupère la valeur du potentiometre
2. on applique une fonction `map()`  pour transformer notre valeur d'entre de `0-1023` en une valeur pour le servo `0-180`
3. on demande au servo d'aller à la valeur actuelle

nous avons maintenant un servo que nous pouvons contrôler facilement



**goal**

nous savons maintenant, créer / manipuler / utiliser des variables, librairie et tous les éléments pour la création de notre station météo. Il nous suffira de charger les bonnes *librairie* et brancher les bons ports sur la carte *arduino*



## jour 3 : mesure et dessin

### Goal

- familiarisation avec un logiciel de dessin vectoriel *inkscape* ([site internet ](https://inkscape.org/en/))
- comprendre le dessin entre réel et numérique et créer des fichiers pour la production numérique
- identifier et dessiner les pièces pour accueillir les capteurs et l' *arduino*

### Inkscape

#### étape 1 : prise de mesures

Pour faire des plans il faut des mesures.
avec l'aide de google et d'une recherche simple nous pouvons déjà trouver des infos

==vérifier tout de mm avec un instrument de mesure que les mesures correspondent à notre équipement ==

![](http://localhost:5000/static/page_images/wtrstn/arduino_uno_dims_lg.png)

avec ceci nous pouvons faire les premiers dessins pour les découpes de placement des éléments  

#### Dessin dans inkscape

dans inkscape et grâce aux outils cercle *![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/weatherStation/ink_tools1.png)* et  rectangle *![](http://localhost:5000/static/page_images/wtrstn/ink_tools.png)*  créons le dessin suivant

en ajustant les mesures dans la barre supérieure de l'application *![](http://localhost:5000/static/page_images/wtrstn/ink_tools3.png)


>![](http://localhost:5000/static/page_images/wtrstn/arduinoDmH.png)

avec cela en tête continuez le dessin pour arriver à notre dessin pour tous les éléments de la station météo

- dht22
- ecran oled
- photoresistance

**![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/weatherStation/1.3_OLED_Lcd.jpg)**  **![](http://localhost:5000/static/page_images/wtrstn/dht22.jpg)**

>![](http://localhost:5000/static/page_images/wtrstn/meteoStation.png)



## jour 4 : code et assemblage

après ces 3 jours de workshop nous avons maintenant tous les éléments utiles à la création de notre station

#### enjeux

réussir à faire le code de la station météo sans *ecrire* de code, grâce à des recherches et copier coller de code depuis internet et de simples modifications  

#### CODE 	

commençons par faire des recherches pour chaque composant

##### les composants

recherche google : *arduino oled*

voici un des résultats possibles (tous très similaires) qui nous offre aussi le code pour le dht

> [http://www.instructables.com/id/The-First-Usage-of-096-I2C-OLED-Display-With-Ardui/](http://www.instructables.com/id/The-First-Usage-of-096-I2C-OLED-Display-With-Ardui/)

suivre le guide et faire le premier test.

une fois effectué extraire le code nécessaire à nos besoins

avec une recherche pour le *dht22* et *arduino*

>[https://create.arduino.cc/projecthub/attari/temperature-monitoring-with-dht22-arduino-15b013](https://create.arduino.cc/projecthub/attari/temperature-monitoring-with-dht22-arduino-15b013)

et enfin pour la *photoresistance*

> [http://www.instructables.com/id/How-to-use-a-photoresistor-or-photocell-Arduino-Tu/](http://www.instructables.com/id/How-to-use-a-photoresistor-or-photocell-Arduino-Tu/)

==ATTENTION NE PAS OUBLIER LA RESISTANCE DE MISE A LA MASSE==
pour en savoir plus [sur le site d'arduino ](https://playground.arduino.cc/CommonTopics/PullUpDownResistor)
**![](http://localhost:5000/static/page_images/wtrstn/220px-Pulldown_Resistor.png)**



après avoir fait les tests de chaque 'demo' et tutoriel vu nous pouvons faire les copiers collers et assemblage de code pour notre station météo


## Code final

	/* Simple weather station made with :
	- Rime
	- Myriam
	- Jason
	- Yanis
	- Jili

	INSTITUE ST DOMINIQUE DE BRUXELLES
	avec Yorick,  Dewi et Julien
	*/


	// Chargement de toutes les libraires pour nos capteurs
	#include <SPI.h>
	#include <Wire.h>
	#include <Adafruit_GFX.h>
	#include <Adafruit_SSD1306.h>
	#include <dht.h>

	// parametres copier coller pour l ecran oled

	#define OLED_RESET 4
	Adafruit_SSD1306 display(OLED_RESET);
	#define NUMFLAKES 10
	#define XPOS 0
	#define YPOS 1
	#define DELTAY 2
	#if (SSD1306_LCDHEIGHT != 64)
	#error("Height incorrect, please fix Adafruit_SSD1306.h!");
	#endif

	// création de l'objet DHT22

	dht DHT;
	#define DHT22_PIN 2

	// création des variables pour stocker nos valeurs relevées
	float hum;  // humidity value
	float temp; // temperature value


	// définition de l'entrée et variable pour la photoresistance
	int LightSensorPin = A0 ;
	int LightVal = 0 ;


	void setup()   {               
	  Serial.begin(9600);
	  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
	  delay(2000);

	  // Clear the buffer.
	  display.clearDisplay();


	  display.setTextSize(1.5);
	  display.setTextColor(WHITE);
	  display.setCursor(0,0);
	  display.println("Bonjour le monde !");
	  display.display();
	  delay(2000);
	  display.clearDisplay();
	}


	void loop(){
	  int chk = DHT.read22(DHT22_PIN);
	  hum = DHT.humidity;
	  temp= DHT.temperature;

	  LightVal = analogRead(LightSensorPin);


	  display.setTextSize(2);
	  display.setTextColor(WHITE);
	  display.setCursor(0,0);
	  display.print("hum:");
	  display.print(hum,1);
	  display.println(" %");
	  display.setCursor(0,20);
	  display.print("deg:");
	  display.print(temp,1);
	  display.println(" C");
	  display.setCursor(0,40);
	  display.print("lum:");
	  display.print(LightVal);
	  display.println("");

	  display.display();

	  delay(1000);
	  display.clearDisplay();
	}
