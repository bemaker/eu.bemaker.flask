#!/usr/bin/python
# -*- coding: <utf-8> -*-
from flask import Flask, render_template, redirect, flash, request, url_for , session
from flask_flatpages import FlatPages
import markdown
from flask_mongoengine import *
from flask_security import Security, MongoEngineUserDatastore, UserMixin, RoleMixin, login_required, current_user, user_registered
from flask_login import user_logged_in

from flask_mail import Mail
from flask_mail import Message
from flask_security.forms import RegisterForm, StringField
from wtforms.validators import DataRequired
import urllib.request as ur
import os, glob, copy, json, random, urllib, math
from bson import ObjectId

from flask_admin import Admin
from flask_admin.form import rules
from flask_admin.contrib.mongoengine import ModelView, filters
from flask_admin.model.fields import InlineFormField, InlineFieldList
from settings import *

from flask_babelex import Babel, gettext

LANGUAGES = {
    'en': 'English',
    'nl': 'Neederlands',
    'fr': 'French'
}

# EVENTBRITE API

# eventbrite_url_requete =  "https://www.eventbriteapi.com/v3/events/search/?q=fablab&expand=organizer&token=AYO4PV2UJHXEBOLGXG74"
# eventbrite_url_requete =  "https://www.eventbriteapi.com/v3/events/48022466490:47017085368/?status=live&expand=organizer&token=AYO4PV2UJHXEBOLGXG74"

def eventBrite_GetData():
    allEvents=[]
    eventIdList = []
    for event in Events.objects():
        eventIdList .append(event.eventbrite_id)
    print(eventIdList)
    for i in reversed(range(len(eventIdList))):
        eventDict={}
        eventbrite_url_requete =  "https://www.eventbriteapi.com/v3/events/"+eventIdList[i]+"/?status=live&expand=organizer&token="+eventbriteToken
        eventbrite_json =  ur.urlopen(eventbrite_url_requete)
        events = json.loads(eventbrite_json.read().decode())
        eventDict.update({'name': events['name']['text']})
        eventDict.update({'desc': events['description']['text']})
        eventDict.update({'img': events['logo']['original']['url']})
        eventDict.update({'url': events['url']})
        eventDict.update({'date': events['start']['local']})
        eventDict.update({'organizer': events['organizer']['name']})
        eventDict.update({'capacity': events['capacity']})
        allEvents.append(eventDict.copy())
    # print(allEvents)
    return allEvents

def set_eventName(id):
    eventbrite_url_requete =  "https://www.eventbriteapi.com/v3/events/"+id+"/?status=live&token=AYO4PV2UJHXEBOLGXG74"
    eventbrite_json =  ur.urlopen(eventbrite_url_requete)
    events = json.loads(eventbrite_json.read().decode())
    event.human_readable_name_of_event = events['name']['text']
    event.save()

DEBUG = True
FLATPAGES_AUTO_RELOAD = DEBUG
FLATPAGES_EXTENSION = '.md'
FLATPAGES_MARKDOWN_EXTENSIONS = ['markdown.extensions.extra', 'headerid', 'toc']

app = Flask(__name__)
babel = Babel(app)
#app.config.from_pyfile('mysettings.cfg')
app.config.from_object(__name__)
pages = FlatPages(app)


@babel.localeselector
def get_locale():
    # if the user has set up the language manually it will be stored in the session,
    # so we use the locale from the user settings
    try:
        language = session['language']
    except KeyError:
        language = 'fr'
    if language is not None:
        return language
    return request.accept_languages.best_match(app.config['LANGUAGES'].keys())

@app.context_processor
def inject_conf_var():
    return dict(
                CURRENT_LANGUAGE=session.get('language',
                                          request.accept_languages.best_match(app.config['LANGUAGES'].keys())))

# Flask Security settings
app.config['SECRET_KEY'] = secret_key
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_RECOVERABLE'] = True
app.config['SECURITY_CHANGEABLE'] = True
# app.config['SECURITY_TRACKABLE'] = True
app.config['SECURITY_PASSWORD_HASH'] = PASSWORD_HASH
app.config['SECURITY_PASSWORD_SALT'] = PASSWORD_SALT
app.config['SECURITY_EMAIL_SENDER'] = 'hello@bemaker.eu'

app.config['MAIL_SERVER'] = MAIL_SERVER
app.config['MAIL_PORT'] = MAIL_PORT
app.config['MAIL_USE_SSL'] = MAIL_USE_SSL
app.config['MAIL_USERNAME'] = MAIL_USERNAME
app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
app.config['MAIL_DEFAULT_SENDER'] = MAIL_DEFAULT_SENDER
mail = Mail(app)

# MongoDB Config
app.config['MONGODB_DB'] = MONGODB_DB
app.config['MONGODB_HOST'] = MONGODB_HOST
app.config['MONGODB_PORT'] = MONGODB_PORT
# app.config['MONGODB_USERNAME'] = MONGODB_USER
# app.config['MONGODB_PASSWORD'] = MONGODB_PSWD

# Create database connection object
db = MongoEngine(app)

class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)

    def __unicode__(self):
        return self.name

class School(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)

    def __unicode__(self):
        return self.name

class Events(db.Document):
    eventbrite_id = db.StringField(required=True, unique=True)
    human_readable_name_of_event = db.StringField()

class Badge(db.Document): # UserMixin ? RoleMixin ?
    # _id = db.ObjectIdField(default = name)
    name = db.StringField(max_length=60)
    #_id = db.ObjectIdField(default = name)
    description = db.StringField()
    default_level = db.StringField(default=1)
    icon = db.FileField(required=False)
    color_hex = db.StringField()
    # user = db.ReferenceField(User, required=False)

# badges = {
#     "cnc": {
#       "lvl": "0"
#     },
#     "laser": {
#       "lvl": "0"
#     },
#     "impression3d": {
#       "lvl": "0"
#     },
#     "maker": {
#       "lvl": "0"
#     },
#     "electronique": {
#       "lvl": "0"
#     }
#   }

badges = [
        {
            "name" : "cnc",
            "lvl" : "0"
        },
        {
            "name" : "laser",
            "lvl" : "0"
        },
        {
            "name" : "impression3d",
            "lvl" : "0"
        },
        {
            "name" : "maker",
            "lvl" : "0"
        },
        {
            "name" : "electronique",
            "lvl" : "0"
        },
        {
            "name" : "badge",
            "lvl" : "0"
        }
]

class User(db.Document, UserMixin):
    active = db.BooleanField(default=True)
    first_name = db.StringField(max_length=255)
    last_name = db.StringField(max_length=255)
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    confirmed_at = db.DateTimeField()
    # badges = db.DictField(default = badges)
    # badges = db.ListField()
    roles = db.ListField(db.ReferenceField(Role))
    #badges =  db.ListField( db.ReferenceField(Badges))
    # dashboard_view_count = db.StringField(max_length=3, default=[0])
    meta = {'strict': False}

class ExtendedRegisterForm(RegisterForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])

    def __unicode__(self):
        return self.name

# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(db, User, Role)

security = Security(app, user_datastore, register_form=ExtendedRegisterForm)

@app.before_first_request
def create_admin():
    user_datastore.find_or_create_role(name='admin', description='Administrator, adminstration view, sees these backend views ')
    user_datastore.find_or_create_role(name='user', description='default for all users, status for students, teachers, anybody who uses the register form')

@app.before_first_request
def create_user():
    # check that we have at least one user that has admin role, if existing, continue
    if not user_datastore.get_user('admin@bemaker.eu'):
        print('creating default admin user because unexisting')
        user_datastore.create_user(email='admin@bemaker.eu', password=admin_pwd, roles=['admin'])

@app.before_first_request
def parse_md():
    import sys
    # cmd = 'cd ' + PATH_TO_REPO_HOME + 'app/md_sources/' + '&&' + 'python ' + PATH_TO_REPO_HOME  + 'pythonParseImgFolder.py .'
    # cd ~/webapps/flask && . venv/bin/activate && cd ~/webapps/flask/eu.bemaker.flask/app/md_sources/ && python pythonParseImgFolder.py . && deactivate
    print('about to parse md files from')
    cmd = 'cd ' + PATH_TO_VENV + ' && . venv/bin/activate' + ' && cd ' + PATH_TO_REPO_HOME + 'app/md_sources/ && python pythonParseImgFolder.py . && deactivate'
    print(cmd)
    os.system(cmd)

@user_registered.connect_via(app)
def on_user_registered(sender, user, confirm_token):
    default_role = user_datastore.find_role("user")
    user_datastore.add_role_to_user(user, default_role)

# @user_logged_in.connect_via(app)
# def attribute_empty_badges(sender, user):
#     print("user logged in")
#     print(user)
    # if badg:
    #     print("user already has badges")
    # else:
    #     print("no badges found, adding default array of badges with values at 0")
#        default_badges =  { "cnc" : { "lvl" : "0"}, "laser" : {"lvl" : "0"}, "impression3d" : {"lvl" : "0"} }
#        User.objects(id = user).update(set__badges = default_badges, upsert=True)

@app.route("/")
@app.route("/index")
def index():
    print(session)
    session['url'] = request.url
    events = eventBrite_GetData()
    articles = (p for p in pages if 'published' in p.meta and 'contenu_statique' not in p.meta)
    # Show the 10 most recent articles, most recent first.
    latest = sorted(articles, reverse=True,
                    key=lambda p: p.meta['published'])
    return render_template("home.html" ,events=events ,articles=latest[:4])

@app.route("/documents")
def documents():
    session['url'] = request.url
    articles = (p for p in pages if 'published' in p.meta and 'contenu_statique' not in p.meta)
    all_docs = (p for p in pages if 'published' in p.meta)
    latest = sorted(articles, reverse=True,
                    key=lambda p: p.meta['published'])
    return render_template("documents.html", articles=latest, all_docs=all_docs)

@app.route("/learn")
def learn():
    session['url'] = request.url
    try:
        session['language']
    except:
        language = 'fr'
    else :
        language = session['language']

    articles = (p for p in pages if 'published' in p.meta and 'contenu_statique' not in p.meta and 'type' in p.meta)
    return render_template("learn.html", articles=articles)

@app.route('/learn/<path:path>/')
def page(path):
    page = pages.get_or_404(path)
    # print(page.toc)
    md = markdown.Markdown(extensions=['markdown.extensions.toc'])
    html = md.convert(path)
    print('coucou' + html)
    # toc = page.keys()
    # print(toc)
    print(page.meta)
    print(session)
    session['url'] = request.url
    print(session)
    return render_template('flatpage.html', page=page, request=request)

quiz_dir = QUIZ_DIR
quizzes = {}
for quiz in os.listdir(quiz_dir):
    quizzes[quiz] = json.loads(open(os.path.join(quiz_dir, quiz)).read())

@app.route('/quizzes')
def quizs():
    session['url'] = request.url
    quiz_names = zip(quizzes.keys(), map(lambda q: q['name'], quizzes.values()))
    returnQuiz_names = list(quiz_names)
    print(returnQuiz_names)
    return render_template('quizzes.html', quiz_names=returnQuiz_names)

@app.route('/quiz/<id>')
def quiz(id):
    if id not in quizzes:
        return abort(404)
    quiz = copy.deepcopy(quizzes[id])
    questions = list(enumerate(quiz["questions"]))
    random.shuffle(questions)
    quiz["questions"] = map(lambda t: t[1], questions)
    ordering = list(map(lambda t: t[0], questions))

    return render_template('quiz.html', id=id, quiz=quiz, quiz_ordering=json.dumps(ordering))

@app.route('/check_quiz/<id>', methods=['POST'])
def check_quiz(id):
    session['url'] = request.url
    ordering = json.loads(request.form['ord'])
    quiz = copy.deepcopy(quizzes[id])
    badge = quiz['badge']
    # print(badge)
    badge = str(badge)
    quiz['questions'] = sorted(quiz['questions'], key=lambda q: ordering.index(quiz['questions'].index(q)))
    answers = dict( (int(k), quiz['questions'][int(k)]['options'][int(v)]) for k, v in request.form.items() if k != 'ord' )
    if not len(answers.keys()):
        return redirect(url_for('quiz', id=id))

    for k in range(len(ordering)):
        if k not in answers:
            answers[k] = [None, False]

    answers_list = [ answers[k] for k in sorted(answers.keys()) ]
    number_correct = len(list(filter(lambda t: t[1], answers_list)))
    badge_ok = math.floor((len(answers_list) / 3) * 2)
    C_user = current_user.get_id()
    print(C_user)
    if number_correct >= badge_ok:
        #award badge
        print('award badge ' + badge)
        updateUser = User.objects.get(id=C_user)
        dbbadge = Badge.objects.get(name=badge)
        print(dbbadge)
        dbbadge = str(dbbadge)
        print(dbbadge)
        dbbadge = json.loads(dbbadge)
        # dbbadge = json.loads(dbbadge)
        print(dbbadge)
        # updateUser.badges = dbbadge
        # updateUser.update(push__badges=dbbadge)
        # updateUser.save(validate=False)
        print(updateUser)
        success_message = gettext(" Felicitation vous avez le badge :  ") + badge +"<i class='material-icons'>sentiment_satisfied</i>"
        flash(success_message, 'alert-success')
    else:
        print('user got less than required amount')
        fail_message = gettext("ohhh non tu n'as pas eu assez de bonne reponses !") + "<i class='material-icons'>sentiment_dissatisfied</i>"
        flash(fail_message, 'alert-danger')

    return render_template('check_quiz.html', quiz=quiz, question_answer=zip(quiz['questions'], answers_list), correct=number_correct, total=len(answers_list))

@app.route("/about")
def about():
    session['url'] = request.url
    try:
        session['language']
    except:
        language = 'fr'
    else :
        language = session['language']
    articles = (p for p in pages if 'published' in p.meta and language in p.meta)
    return render_template('about.html', articles=articles)

@app.route("/program")
def program():
    session['url'] = request.url
    try:
        session['language']
    except:
        language = 'fr'
    else :
        language = session['language']
    articles = (p for p in pages if 'published' in p.meta and language in p.meta)
    return render_template('program.html', articles=articles)

@app.route("/partners")
def partners():
    session['url'] = request.url
    try:
        session['language']
    except:
        language = 'fr'
    else :
        language = session['language']
    articles = (p for p in pages if 'published' in p.meta)
    return render_template('partners.html', articles=articles)

@app.route("/contact")
def contact():
    session['url'] = request.url
    return render_template('contact.html')

@app.route("/succes" , methods=["GET", "POST"])
def success():
    if request.method == 'POST':
            reply_to = request.form.get('email')
            message = request.form.get('message')
            subject = request.form.get('subject')
            emailS = request.form.get('email')
            message = emailS + "\n" + subject + "\n" + message
            # print(message)
            msg = Message(recipients=["hello@bemaker.eu"],
                          body = message,
                          subject = "Contact from outerspace (or website)",
                          sender = "no-reply@bemaker.eu" )
            mail.send(msg)

    return render_template('succes.html')

@app.route("/event")
def event():
    session['url'] = request.url
    events = eventBrite_GetData()
    # query = ""
    # for event in Events.objects():
    #     query += event.eventbrite_id + " + "
    # print(query)
    return render_template("event.html" , events = events )

@app.errorhandler(404)
def not_found(error):
    return render_template('error.html'), 404


@app.route('/dashboard')
@login_required
def dashboard():
    session['url'] = request.url
    C_user = current_user.get_id()
    print('user: ' + C_user)
    #db_user = User.objects.get(id=C_user).update(inc__dashboard_view_count=1, upsert=True)
    message = 'Welcome to your dashboard ' + current_user.get_id()
    flash(message ,  "alert-info")
    return render_template('dashboard.html', user=C_user)

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('security.login', next=request.url))

@app.route('/deploy', methods=["GET", "POST"])
def deploy():
    import subprocess
    import shlex
    import time
    cmd	= DEPLOY_COMMAND
    parsed_cmd = shlex.split(cmd)
    # print(parsed_cmd)
    subprocess.call(parsed_cmd)
    time.sleep(5)
    return redirect(url_for('index'))

@app.route('/language/<language>')
def set_language(language='fr'):
    session["language"] = language
    return redirect(session['url'])

# class UserForm(form.Form):
#     name = fields.StringField('Name')
#     email = fields.StringField('Email')
#     password = fields.StringField('Password')

class UserView(ModelView):
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('security.login', next=request.url))

    def has_role(self, role):
        return role in self.roles

    column_list = ('active', 'first_name', 'last_name', 'email' , 'roles')
    column_sortable_list = ('email')


    form_ajax_refs = {
        'roles': {
            'fields': ['name']
        }
}

class RoleView(ModelView):
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('security.login', next=request.url))

class EventView(ModelView):
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('security.login', next=request.url))


# APP RUN

admin = Admin(app, name='bemaker', template_mode='bootstrap3')
admin.add_view(UserView(User))
admin.add_view(RoleView(Role))
# admin.add_view(ModelView(Badge))
admin.add_view(EventView(Events))


if __name__ == '__main__':
    app.run()
