#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Flask Security settings
secret_key = 'set-a-super-strong-pw'

PASSWORD_HASH = 'bcrypt'
PASSWORD_SALT = 'great_salt'

# mail server settings
MAIL_SERVER = 'mail.smth.net'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'noreply@smth.net'
MAIL_PASSWORD = ''
MAIL_DEFAULT_SENDER = ''

# MongoDB Config
MONGODB_DB = 'bemaker'
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
MONGODB_USER = 'USER'
MONGODB_PSWD = 'DBPW'

QUIZ_DIR = '/home/colm/git/bemaker/eu.bemaker.flask/app/quizzes/'

DEPLOY_COMMAND = 'sh /home/colm/git/bemaker/eu.bemaker.flask/deploy.sh'

PATH_TO_REPO_HOME = '~/git/bemaker/eu.bemaker.flask/'
PATH_TO_VENV = '~/git/bemaker/eu.bemaker.flask/'

admin_pwd = 'setsomethingverystrong'

#eventBrite api
eventbriteToken ='API_TOKEN'
# admin user for bemakerDb
admin_pwd = 'strongpassword'
