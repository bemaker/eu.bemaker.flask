...
title: Découpe laser
published: 2010-12-24
quiz:
cat:
folderImg: .
desc: Découper via un laser
type: tutoriel
...

[TOC]

# La découpe laser



Découper de la matière grâce à de la lumière ? C’est le principe même de la découpeuse/graveuse laser. Découvres ici son fonctionnement, ses utilisations possibles et apprend à créer ton design pour ensuite le graver.

Pars à la conquête de ton badge “Laser”!

## Principe et fonctionnement

### Et… Lumière!
La découpe laser est un procédé de fabrication basé sur l’utilisation d’un laser, autrement dit d’un rayon de lumière. Une fois concentré sur une petite surface, ce rayon dégage une forte énergie capable de découper la matière. Cette concentration de la lumière en un point est permise par un jeu de miroir et de lentille après la sortie du tube/diode.


Faites l’expérience : prenez une loupe et mettez la sous le soleil. La loupe va focaliser les rayons du soleil en un point et créée une forte chaleur, capable de démarrer une flamme. Le laser fonctionne sur le même principe. La puissance du faisceau lumineux se concentre en un point et brûle, à cet endroit, la matière.

### Différentes technologies

Il existe trois grandes familles de laser:


- Laser à tube CO2

Ce laser est le plus répandu et le plus abordable. Il permet de travailler avec une grande diversité des matériaux.

Attention, la sécurité avant tout: ces lasers utilisent des alimentations électriques très haute voltage (plusieurs dizaine de milliers de volts). Il ne faut en aucun cas toucher les câbles lors du fonctionnement de la machine!
[Clique ici pour en savoir plus](https://en.wikipedia.org/wiki/Carbon_dioxide_laser)

- Laser à Yag

Ce laser est principalement utilisé dans le milieu médical (traitement esthétique et/ou ophtalmologique) et dans l’industrie lourde (découpe, soudure en acier et superalliage).
[Clique ici pour en savoir plus](https://fr.wikipedia.org/wiki/Laser_Nd-YAG)

- Diode laser

Ce laser fonctionne sur le même principe mais est un peu moins puissant. Il coûte également un peu moins cher. Il est utilisé dans la fabrication de nombreux produit de grande consommation.  
[Clique ici pour en savoir plus](https://fr.wikipedia.org/wiki/Diode_laser)


Consigne sécurité : Attention, le faisceau laser est très puissant. Ne jamais mettre ses mains dans la zone de découpe lors du fonctionnement du laser et  ne jamais regarder le laser sans lunette ou lorsque le  capot  est ouvert sous peine de perdre la vue de façon quasi instantanée !


## Utilisation


## Imaginez, dessinez et gravez

On distingue trois étapes dans la gravure/découpe laser :
- Créer la forme, l’objet ou le design

- Convertir le design en langage Gcode (dans la majorité des cas)

- Envoyer les commandes à la machine

### Logiciel pour le design

Il existe plusieurs logiciels permettant de concevoir son design.

Nous pouvons, citer, parmis d’autres, ces logiciels open-sources et gratuits:
- [Librecad](http://librecad.org/cms/home.html) : dessin 2D cad / gratuit | openSource | osx,linux,windows
- [Inkscape](https://inkscape.org/en/) :  dessin 2D  et illustration vectoriel / gratuit | openSource | osx,linux,windows (site internet)
- [qCad](https://qcad.org/en/) :  Dessin 2D cad / free & pro version | openSource | osx,linux,windows (site internet)


[Pour démarrer avec Inkscape](https://inkscape.org/fr/apprendre/didacticiels/)

[Pour démarrer avec libreCad/Qcad](http://www.linux-france.org/article/appli/infographie/qcad.htm)

### Logiciel des commandes machines et interface machines

Certains logiciels permettent de lancer les commandes sur les machines et de traduire le design en GCode;

- [Inkscape](https://jtechphotonics.com/?page_id=2012) avec Laser Tool Plug-in -free/Opensource
- [LaserWeb3](https://github.com/LaserWeb/LaserWeb3) -free/Opensource
- [LasaurApp](https://github.com/stefanix/LasaurApp) – free/Opensource (soutenue par lasersaur.com opensource laser cutter )


>   Qu’est-ce que le Gcode?
Le Gcode est un langage de programmation utilisé par les machines de création technique (Imprimante 3D, CNC, laser, etc.). Ce code est composé de “coordonnées” XYZ qui indique à la machine où se positionner.  
