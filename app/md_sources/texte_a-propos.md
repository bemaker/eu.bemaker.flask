...
title: A propos
published: 2018-08-23
contenu_statique: oui
fr: oui
folderImg: .
...

<div markdown=true class="col-xs-12 col-sm-12 col-md-8 offset-md-2 col-lg-8  offset-lg-2">

BeMaker souhaite **inspirer et former les jeunes à la création par les nouvelles technologies**. Équipé des machines issues des Fablabs (Imprimante 3D, Laser, CNC, etc.), notre tech-truck se rend gratuitement dans les écoles. Grâce à la découverte de ces technologies et de leurs capacités de création presque infinies, nous souhaitons **stimuler la créativité des jeunes et les ouvrir à un nouveau monde des possibles**.

Entièrement financé, notre programme est **gratuit et accessible à toutes les écoles**. Nous oeuvrons  pour un système scolaire équitable où tous les jeunes peuvent se former aux nouvelles technologies et se préparer aux enjeux sociétaux de demain.

BeMaker participe ainsi à une **intégration du numérique à l'école**; une intégration réfléchie, critique et surtout créative. Les technologies présentes dans notre fablab mobile sont mises au service de la créativité pour inventer et concevoir des objets tangibles du quotidien. Un lien entre le monde numérique et le monde réel permettant aux jeunes de passer de consommateur de technologie à créateur.

<div class="spacer"></div>

### L'équipe BeMaker :

<div class="spacer"></div>

</div>



<div markdown=true class="person col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <a href="mailto:&#121;&#111;&#114;&#105;&#99;&#107;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;">
  <img class="person-bubble" src="../static/img/yorick.jpg">
  <p>Yorick Schetgen : &#121;&#111;&#114;&#105;&#99;&#107;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;</p></a>
</div>

<div markdown=true class="person col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <a href="mailto:&#106;&#117;&#108;&#105;&#101;&#110;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;">
  <img class="person-bubble" src="../static/img/julien.jpeg">
  <p>Julien Dutertre : &#106;&#117;&#108;&#105;&#101;&#110;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;</p></a>
</div>

<div markdown=true class="person col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <a href="mailto:&#109;&#97;&#114;&#103;&#111;&#116;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;">
  <img class="person-bubble" src="../static/img/placeHolder.png">
  <p>Margot Brulard : &#109;&#97;&#114;&#103;&#111;&#116;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;</p></a>
</div>

<div markdown=true class="person col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <a href="mailto:&#99;&#111;&#108;&#109;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;">
  <img class="person-bubble" src="../static/img/placeHolder.png">
  <p>Colm O'Neill : &#99;&#111;&#108;&#109;&#64;&#98;&#101;&#109;&#97;&#107;&#101;&#114;&#46;&#101;&#117;</p></a>
</div>
