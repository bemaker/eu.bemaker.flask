# parser that is ran to process .md files one by one
# $ python pythonParseImg.py inputMdFile.md
import re
import sys
from yaml import load, dump
import yaml
import os

inputMd = open( sys.argv[1] , 'r')
fileName = sys.argv[1].partition('.')
print(fileName[0])

inputMd = inputMd.read()
outputFile = open('_PARSED_'+fileName[0]+'.md',"w")
print(outputFile)
yamlData =yaml.load(str(inputMd.partition('...')[2].partition('...')[0]))
print(yamlData)
folderName = yamlData['folderImg']
outputMd = re.sub(r'(?:!\[(.*)\]\()(.*)(\/)(.*)(?:\))', r'![\1](http://localhost:5000/static/page_images/%s/\4)'%(folderName), inputMd )
outputMd = re.sub(r'(\.\.\.)\n','', outputMd )
outputFile.write(outputMd)
outputFile.close()
