...
title: Partners
published: 2018-09-10
contenu_statique: oui
fr: oui
folderImg: .
partnerLogos:
  - img/Logo-DBSF.png
  - img/innoviris.jpg
  - img/fortis.jpg
  - img/Intel-logo.svg
...

Ils ont soutenus le projet de BeMaker:
