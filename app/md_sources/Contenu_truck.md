...
title: Contenu Machines truck
published: 2018-09-05
contenu_statique: oui
folderImg: .
...

[TOC]

# VISITE DU BUS (+/- 30mins)

## avant d'entre dans le bus

### Securite

prevenir les eleves que les machines sont fragiles et dangeureuse pour eux

- imprimante 3d 200deg+ pour la tete d'impression
- Cnc outil coupant avec moteur a 30000rpm+ qui peut couper du bois (et donc les doigts)
- laser rayon puissant pouvant ET CE SANS AUCUNE DOULEUR RESSENTIE cree des point et taches IRREMEDIABLE en l'espace de quelque seconde dans les yeux

### Organisation du bus

le bus est divise en 2 espaces

1. contruction / fabrication numerique
2. Robotique / programation

1. CONTRUCTION ASSISTE PAR ORDINATEUR

etablie de 3 machines dans l'ordre:  

- imprimante 3d
- qcnc
- laser

l'imprimante 3d

objecif

: comprendre le fonctionnement de la machine du ficheir 3d (volume) a impression

mettre a jour la suite logiciel pour ce faire : Creation fichier 3d > export et decoupage en tranches (slicer) > impression

1. poser la question de qui a deja vu/entendu/utilise une imprimante 3d et d'essayer dexpliquer le system
2. mettre ne place une separation entre OUTIL et MACHINE
3. outil imprimante 3d , faire le lien avec un pistolet a colle

 tube de colle | gachete | pointe chauffante

 bobine de filament | extruder | tete impression

1. introduction au moteur pas a pas (tres haut niveau / moteur contrlolable tres precisement )
2. analogie avec disque de papier superpose en grand nombre pu faire un cylindre
3. explication du logitiel de slicing (trancheur) qui cree les coupes etages par etages
4. appartenance a la famille des machines ADDITIVE

CNC / fraiseuse numerique

objectif

: comprendre les machines substractives (retirer de la matiere au cours du process de fabrication )

faire des liens tangibles entre les cours de math (repere du plan / systeme de coordonne x,y)

introduction au gCode (si eleves receptif)

1. introduction de la famille SUBSTRACTIVE
2. Toujours faire la sepration entre la machine et l'outil
3. explication de la fraiseuse (a l'aide de la fraise de dentiste si besoin) et donc de l'outil en presence
4. faire demonstration des axes x,y et lien avec le repere Cartesien / orthonorme
5. simplifier le process et le definir comme une machine a relier des points (cf les dessins pour enfant a points) possibilite de parler des ardoises magic (2 molettes pour respectivement x et y)
6. DEMO (LaserWeb ouvrir fichier Gcode sur le bureau "DEMO")

Laser

1. reprendre les points de la cnc pour la machine (repere x,y / mouvement de points en point ....)
2. introduction a la lumiere laser
   - rayon puissant
   - lumiere avec rayon parallele
   - si eleve interesse aller plus loin
3. experience de la loupe au soleil
   - faire un appel a qui a fait lexperience et demander le fonctionnement de celle ci
   - montrer la partie optique sur le laser pour faire echo avec l'histoire de la loupe
4. DEMO
   - lancer elekCam depuis le bureau
   - aller dans text engrave et ecrire un texte (beMaker par exemple)
   - connecter la machine ** HORS TENSION **
   - DISTRIBUTION DES LUNETTES DE PROTECTION
   - un fois la connection etablie alumer la machine
   - faire les 0,0 machine
   - placer une cartonette
   - lancer la gravure

2. ROBOTIQUE

objectif

:  comprendre la fonction d'un controleur (micro controleur )

introduction a la notion de programmation

mise en evidence de la logique input / output

1. demo lineFollowing avec Mbot
2. discution sur les elements qui font un robot
3. introduction de la plaque de developpement Arduino et microcontroleur
4. demo Mblock
5. mise en evidance de la presence de micro controleur dans toutes les machines precedentes et tout autre machine usuel
6. fin et retout en classe pour debut activitee
