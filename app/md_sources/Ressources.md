...
title: ressources
published: 2010-12-24
cat: elec
folderImg : ressources
desc: toutes les ressources pour bien demarrer
...

# WHERE TO START ?

##SOFTWARE RESSOURCE
###2D drawing / plan / edition

 *![fuuuu](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/inskape_ico.png)* **INKSCAPE**

 ![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/inkscape.jpg)

*licence*  : free / opensource
*os* : linux / osx / win  

*use* vector graphic design software - illustrator like  

>
- illustration / logo / plan for cnc and laser cut  
- could be use as gcode generator with **plugin**
- lots of avialable plugin (box / gear / puzzle creation )
- import `.ai` from illustrator

[inkscape web site ](https://inkscape.org/en/)  
[getting started ](https://inkscape.org/en/learn/)


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/librecad_ico.png)* **libreCad**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/librecad.jpeg)

*licence* free / opensource
*os* linux / osx / win  

*use*:  Cad drawing / techinical  - autocad like

>
- create multiple `DXF` version to export  

[libreCad web site ](https://librecad.org/)  
[getting started ](https://wiki.librecad.org/index.php?title=Main_Page)


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/qcad_ico.jpeg)* **Qcad**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/qcad.jpg)

*licence*  opensource / not Free (*free version avialable*)
*os* linux / osx / win  

*use* Cad drawing / technical drawing ) - autocad like

>
- import / export `dwg`
- isometric tools
- active developpement and plugin
- `CAM` plugin for Gcode generation

[Qcad web site ](https://www.qcad.org/en/)  
[getting started ](https://www.qcad.org/doc/qcad/latest/reference/en/)

*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/gimp_ico.jpeg)* **Gimp**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/gimo.png)

*licence* free / opensource
*os* linux / osx / win  

*use* image manipulation / pixel drawing - photoshop like

>
- layer and mask
- actively devellope

[gimp web site ](https://www.gimp.org/)  
[getting started ](https://www.gimp.org/tutorials/)

*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/scribus_ico.jpeg)* **Scibus**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/scribus2.png)

*licence* free / opensource
*os* linux / osx / win  

*use* print and book making / edition sofware - inDesign like

>
- prepress tool
- gabarit

[scribus website ](https://www.scribus.net/)  
[getting started ](https://wiki.scribus.net/canvas/Help:TOC)

###3d software

*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/blender_ico.png)* **Blender**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/blender.jpg)

*licence* free / opensource
*os* linux / osx / win  

*use* 3d Modeling / animation / image creation.

>
- light and powerfull
- lots of ressources online
- news 3d printing pannel
- all include (modeling texturing rendrering )
- can bu use to generate 3d gcode with `blenderCam` ==only' on specific blender version==


[blender website ](https://www.blender.org/)  
[getting started ](https://www.blender.org/support/tutorials/)


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/openscad_ico.jpeg)* **OpenScad**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/openscad.jpeg)

*licence* free / opensource
*os* linux / osx / win  

*use* 3d Modeling / part creation /

==code generated drawing==

>
- light and powerfull
- parametric solid modeling


[openscad website ](http://www.openscad.org/)  
[getting started ](http://www.openscad.org/documentation.html)


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/freecad_ico.jpeg)* **FreeCad**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/freecad.jpeg)

*licence* free / opensource
*os* linux / osx / win  

*use* 3d drawing / parametric design / part creation

>
- 2d / 3d software included
- parametric solid modeling
- new `path` workbench for Gcode creation (cnc)


[freecad website ](https://www.freecadweb.org/)  
[getting started ](https://www.freecadweb.org/wiki/Getting_started)


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/fusion_ico.jpeg)* **fusion360 autodesk**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/fusion.jpeg)

*licence*  mountly plan  / close source / *student free version*
*os* ~~linux~~ / osx / win  

no plan for a `linux` version.

*use* 3d Modeling / part creation / parametric

>
- powerfull
- parametric solid modeling
- widely used
- `CAM` module include


[autodesk Fusion360 website ](https://www.autodesk.com/products/fusion-360/students-teachers-educators)



*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/tinker_ico.png)* **tinkerCad**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/tinkercad.jpg)

*licence* free / plan
*os* Web Browser based

*use* 3d Modeling / part creation /


>
- online / no installation require
- parametric solid modeling


[tinkercad web site ](https://www.tinkercad.com/)  
[getting started ](https://www.tinkercad.com/learn/)


### CAM / machine control

#### CNC / Laser

*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/laserweb_ico.png)* **LaserWeb4**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/laserweb.png)

*licence* free / opensource
*os* linux / osx / win  

*use* Cnc / laser gcode creation & machine control (GBRL 1.1f min)


>
- light and powerfull
- All include
- easy to use
- control both *CNC* and *Laser* (`GRBL` based)


[laserWeb4 gitHub page ](https://github.com/LaserWeb/LaserWeb4)  


**MakerCAM**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/makercam-profile.png)

*licence* free
*os* webBrowser app

*use* CNC 2.5D gcode generator

==need FLASH to operate ==

>
- simple and easy to use
- may cause problem in mm due to too much decimal. can be fix with python script [truncate](https://github.com/jhessig/metric-gcode-truncator)  


[makercam website ](http://makercam.com)  


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/bcnc_ico.JPG)* **Bcnc**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/bCNC.png)

*licence* free / openSource
*os*  linux / osx / windows

*use* CNC 2.5D gcode generator

base on `python`

>
- lot of feature (bed leveling /gcode editor / gcode generator / various tools )
- grbl Based machine (`1.1f` min)



[Bcnc gitHub Page ](https://github.com/vlachoudis/bCNC)  

#### 3D print

*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/cura.png)* **Cura**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/cura.jpg)

*licence* free / openSource
*os*  linux / osx / windows

*use* Generate 3d printer gcode file and control

>
- lots of predefine printer parametre
- lots of users

[ultimaker Cura website ](https://ultimaker.com/en/products/ultimaker-cura-software)


*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/sllicer_ico.jpeg)* **Slic3r**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/slicer.jpg)

*licence* free / openSource
*os*  linux / osx / windows

*use* Generate 3d printer gcode file and control

>
- offert full control on parameter  
- lots of users and documentation
- really good new feature in the prusa edition

==the new version is now from Prusa research==


[Slic3r Official website ](http://slic3r.org)
[Slic3r Prusa edition website ](https://www.prusa3d.com/slic3r-prusa-edition/)

*![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/octoprint_ico.png)* **Octoprint**

![](/home/zvevqx/LAB/work/clients/Bemaker/tuto/ressource/img/octo-main.png)

*licence* free / openSource
*os*  linux / osx / windows

*use* Print server for 3d printer / printer control / gcode creation from server (cura 1.5 || slicer )

>
- full printer monitoring  
- pugin to add functionality
- run on a `raspberryPi` and doest require a laptop or sd cad to operate machine (all can be done via local network)

[octoprint website](https://octoprint.org/)

## objet ressource and download

####3dprint / cnc / laser files

**thingiverse**

- 3d files (stl) objet and part
- laser and cnc objet and part
- some parametric model (*openScad*)

[thingiverse](https://www.thingiverse.com/)


**youimagine**

- 3d files (stl) objet and part
- laser and cnc objet and part


[youimagine](https://www.youmagine.com/)

**myminifactory**

- 3d files ready to print

[myminifactory](https://www.myminifactory.com/)

####Tutorial / howto

**instructable**

- all the tutorial you can think of
- arduino
- diy
- robot ...

[instructable ](https://www.instructables.com/)
