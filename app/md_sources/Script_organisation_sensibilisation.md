...
title: Script organisation sensibilisation
published: 2018-09-05
contenu_statique: oui
folderImg: .
...

[TOC]

# Tech Truck en visite dans les écoles - Script 2017-2019


## Préparation du Truck avant le départ

Ouvrir la porte arrière du  Truck


Prendre le matériel situé à dans un local fermé à clé:

Dans le truck, vérifier la présence de...

- Chargeurs robot
- Ordinateur Animateur
- Allonge 40 mètres
- Rallonge ordinateur 10 mètres



Avant de démarrer, vérifier que ...

- le matériel soit bien au sol et bien fixé
- les roues des comptoirs soient bien bloquées

**ATTENTION : NE JAMAIS DEBLOQUER LES ROUES DU COMPTOIR**

Refermer la porte arrière à clé


EN AVANT TOUTE, DIRECTION L'ÉCOLE!


## Arrivée à l'école et préparation avant l'activité


Garer le truck à l'emplacement indiqué sur la fiche récapitulative (Si l'endroit n'est pas disponible (grille ou autre problème), garez vous provisoirement )

Trouver la  personne de contact  (cfr fiches récap) dans l'école

Garer le truck de manière définitive

Ouvrir le truck

Brancher la rallonge  dans l'école

Brancher la prise du truck dans la rallonge (prise blanche)

Ranger la rallonge sous le comptoir n°1

Allumer ordinateur Animateur (bouton en bas à droite) avec le Mot de passe : bemaker

ATTENTION: TOUJOURS BIEN VÉRIFIER LE NIVEAU DE BATTERIE DU PC ANIMA ET SI BESOIN LE BRACHER


Controller/installer chaque élément du Truck:

1. Imprimante 3D
2. Laser
3. CNC
4. Robot
5. Décoration diverses

### Imprimante 3D

Sortir l'imprimante de la caisse et la mettre sur le comptoir 2

Brancher la  prise,

Vérifier que le filament ne soit  pas emmêlé,

 Allumer la machine (bouton on sur le côté)

Appuyer sur le bouton menu (bouton rouge au milieu)

Suivre le chemin suivant : Menu →  Play → ext → choix du fichier (exemple : chien. Gco)

La machine chauffe et lance  l'impression au bout de  10 minutes maximum

 ### laser

Vérifier que la machine est éteinte

Brancher le câble USB sur la laser et sur le PC

Lancer l'application Laserweb située sur le bureau

Pour le fonctionnement du software laserweb voir [laserweb.pdf](/static/pdfs/laserweb.pdf)

Une fois la machine connectée à laserweb, allume le laser

ATTENTION: NE JAMAIS ALLUMER LE LASER AVANT DE L'AVOIR CONNECTÉ

Vérifier le fonctionnement de la laser: dans onglet control:

⁻ vérifier le mouvement avec les boutons X+ X- Y+ Y-
- vérifier le laser avec bouton laser test : donne une lumière bleu

ATTENTION : NE JAMAIS RETIRER LE CARTON DE PROTECTION EN DESSOUS DE LA LASER!

Éteindre la machine

Déconnecter dans l'onglet comms → disconnect

### CNC

Retirer l'USB de la laser  et brancher le sur la CNC


Lancer l'application Laserweb située sur le bureau

Pour le fonctionnement du software laserweb voir [laserweb.pdf](/static/pdfs/laserweb.pdf)

Une fois la CNC connectée, allumer la

Vérifier le fonctionnement de la CNC: dans onglet control:

vérifier le mouvement avec les boutons X+ X- Y+ Y- Z+ Z-

ATTENTION : TOUJOURS BIEN VERIFIER Z+ ET Z- PAR RISQUE D'ABIMER LA FRAISEUSE!

Charger un fichier : onglet “files” →   section G code--> cliquer sur icone ouvrir dossier


Choisir  fichier “cnchelloworld.gcode”

Placer l'origine à la main (malette xyz sur la machine): l'origine est en bas à gauche (comme sur le fichier) quand est en face à la machine

Définir l'origine sur la machine : Onglet control, cliquer sur “Set zero”

ATTENTION : TOUJOURS APPUYER SUR SET ZERO UNE FOIS QUE L'ORIGINE EST MISE MANUELLEMENT

### Robot

Placer robot mboot sur “line following”  sur le comptoir 3

### Déco

Sortir exemple d'impression 3D et les placer sur comptoir 2

 Ouvrir le Pitop sur comptoir 3 (ne pas  le démarrer, juste montrer l'intérieur)

TOUT EST PRÊT ! DIRECTION LA CLASSE

Prendre  les caisses 2X PC + 2Xrobot

Fermer le bus (à clé ) et aller chercher les élèves en classes

## Démonstration Tech Truck


### Introduction en classe

Introduire

- le projet FMB
- les 6 partenaires
- principe de création Tech et de maker
- Fablab

Expliquer programme de l’activité aux élèves

Se diriger avec le groupe vers le Tech Truck

### ouverture tech truck


Ouvrir la porte latérale du Tech Truck


ATTENTION FAIRE ATTENTION AUX ENFANTS À L'OUVERTURE DE LA PORTE!


### Animation

Pour le contenu pédagogique détaillé de l’animation, voir le document contenu-machine-truck reprennant les informations principales pour chaque machine .

ATTENTION: AVANT LANCEMENT DE LA LASER, NE PAS OUBLIER DE DONNER LUNETTES DE PROTECTION AUX ENFANTS

 ### Fin de la démonstration

Si l’animation continue l’après midi, relancer l’imprimante 3D

Éteindre toute les machines et retirer le câble.

Prendre le PC animateur, le câble d’alimentation et le cable HDMI

Refermer la porte latérale du truck

Fermer le truck à clé

C’EST PARTI, DIRECTION LA CLASSE AVEC LES ÉLÈVES!





 ## Activité en classe

Pour le contenu pédagogique détaillé de l’animation, voir fiche d’activité.


## Fin et rangement

Ranger tous les PC + chargeurs et tous les robots dans les caisses adéquates

Si le truck ne reste pas plusieurs jours à l’école, retourner avec les caisses dans le bus

Si le truck reste en stationnement plusieurs jours dans la cour de l’école, sécuriser les PC et robot dans un local de l’école fermé à clé.

Bien vérifier que toute les machines sont débranchées

Débrancher la rallonge dans l’école

Débrancher le multiprise

Ranger la rallonge et le multiprise

Ranger l’imprimante 3D dans sa boite de transport et descendre les têtes d’impressions vers le bas

Ranger la caisse de transport de l’imprimante 3D en sécurité

Vérifier que toutes les roues de comptoirs soient bien bloquées

Fermer la porte à clé du Truck

Retour vers l’endroit de parking fixé à l’avance avec la coordination

Sortir les caisses de PC et de robot pour les sécuriser.

Fermer le camion

Si besoin, passer la clé à la personne nécessaire  


**FIN, BRAVO ET MERCI POUR CETTE BELLE ACTIVITÉ**
