...
title: Programme
published: 2018-08-23
contenu_statique: oui
fr: oui
folderImg: .
...

<div markdown=true class="col-xs-12 col-sm-12 col-md-8 offset-md-1 col-lg-8  offset-lg-1">

<div class="spacer"></div>

Notre programme combine une découverte du tech truck et d’un atelier à l’école, des tutoriels en ligne et des ateliers de suivi via un réseau de fablab et autres acteurs de formation au numérique.

### Tech-truck à l’école

Notre activité de sensibilisation se compose de la **visite du tech-truck** et d'un
**atelier participatif** en classe sur une durée totale de 2H30 (trois périodes de 50 minutes). Seuls une place de parking, une prise électrique à moins de 40 mètres et un local sont nécessaires à notre visite.

Dans un premier temps, les élèves visitent le Tech-Truck. Les machines y sont installées et leurs fonctionnements expliqués. L'imprimante 3D, similaire au pistolet à colle ? Des coordonnées X Y Z, comme au cours de math,  pour la commander ? Rien de sorcier dans tout ça  !

</div>

<div markdown=true class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <img class="w-100" src="../static/img/photos/bemaker_imgs/0navid.jpg">
</div>

<div markdown=true class="col-xs-12 col-sm-12 col-md-8 offset-md-1 col-lg-8 offset-lg-1">

Il s'agit avant tout de **démystifier** les technologies de création mais aussi d'**inspirer** le jeune en lui montrant différent projet et ainsi libérer son imagination et sa créativité .

Ensuite, les élèves retournent en classe pour un atelier participatif où ils pourront eux même tester activement ces nouvelles technologies.

</div>

<div markdown=true class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <img class="w-100" src="../static/img/photos/bemaker_imgs/0makertown1.jpg">
</div>

<div markdown=true class="col-xs-12 col-sm-12 col-md-8 offset-md-1 col-lg-8 offset-lg-1">



### Tuto, quizz et projet en ligne

Via notre **plateforme web**, nous proposons aux jeunes de continuer leur parcours makers. Des tutoriels en ligne leur permettront d'approfondir les techniques appréhendées lors de la visite du tech truck(impression 3D, laser, CNC, etc.) et des quizzs challengeront leur apprentissage.
Grâce à des exemples de projets à réaliser, le maker en herbe peut également se lancer dans la réalisation de prototype concret.


### Le réseau maker en Belgique
Finalement, nous proposons également aux jeunes de s’inscrire à des **ateliers et événements makers** accessibles à proximité de chez lui.

Ce suivi est rendu possible grâce à la mise en réseau de nombreuses associations travaillant dans le secteur numérique et de la jeunesse. Découvre-les ici.

<div class="spacer"></div>


</div>


<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="clear:both;min-height:250px;height:auto">
  <div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden;">
    <img style="max-width:100%;display: block;"src="../static/img/openfab.png">
  </div>
  <div class="name" style="text-align: center ;margin-bottom: 15px;">
  <h2>OpenFab</h2>
  <p>        blrd de la courrone 237 <br /> 1050 Ixelles<br />
        <a href="http://openfab.be/" target="_blank"> site internet</a></p></div>
  </p>
  </div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background:white">
        <img style="max-width:100%;display: block;margin: auto;"src="../static/img/imal.png">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>IMAL</h2>
<p>        30 Quai des Charbonnages<br />Bruxelles 1080<br />
        <a href="http://imal.org/en/" target="_blank">site internet</a></p></div>
</p></div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background: white">
        <img style="max-width:60%;display: block;margin:auto;"src="../static/img/logos-fablabke-clef-v6.png">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>Fablab’ke</h2>
<p>        Maison des Cultures et de la Cohésion Sociale <br />Molenbeek-Saint-Jean<br />
        <a href="http://fablabke.castii.be" target="_blank">site internet</a></p></div>
</p></div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background: white">
        <img style="max-width:100%;display: block;margin:auto;"src="../static/img/coderdojo.jpeg">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>CoderDojo</h2>
<p>        <a href="https://www.coderdojobelgium.be/fr" target="_blank">site internet</a></p></div>
</p></div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background: white">
        <img style="max-width:100%;display: block;margin:auto;"src="../static/img/FabLabULB.jpg">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>Fablab ULB</h2>
<p>        Rue du Belvédère 21 <br />Ixelles, 1050<br />
        <a href="https://www.fablabs.io/labs/fablabulb" target="_blank">site internet</a></p></div>
</p></div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background: white">
        <img style="max-width:100%;display: block;margin:auto;"src="../static/img/flablablol.png">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>Fablab Bruxelles</h2>
<p>        RuCampus Kaai Nijverheidskaai 170 <br />1070 BRUSSEL<br />
        <a href="http://fablab.hylas.be/fablab-english/" target="_blank">site internet</a></p></div>
</p></div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background: white">
        <img style="max-width:100%;display: block;margin:auto;"src="../static/img/microfactory-e1512380616266.png">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>Micro Factory</h2>
<p>        55 quai Fernand Demets <br />1070 Anderlecht<br />
        <a href="http://microfactory.be/" target="_blank">site internet</a></p></div>
</p></div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="min-height:250px;height:auto">
<div class="facePic" style="width:200px; height:200px;border-radius: 10%;margin:auto; overflow: hidden; background: white">
        <img style="max-width:100%;display: block;margin:auto;"src="../static/img/robot-1d741827.png">
      </div>
<div class="name" style="text-align: center ;margin-bottom: 15px;">
<h2>techies lab</h2>
<p>        <a href="http://techieslab.org/" target="_blank">site internet</a></p></div>
</p></div>
</div>
