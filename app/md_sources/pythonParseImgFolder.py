# parser that is ran in md_sources folder to output multiple parsed .md files to app/pages folder
# $ python pythonParseImgFolder.py .
import re
import sys
from yaml import load, dump
import yaml
import os
from glob import glob

PATH_TO_REPO_HOME = '/home/zvevqx/LAB/work/www/eu.bemaker.flask/'


print('parsing all .md files in md_sources folder')

inputMd = glob('*.md')

for file in inputMd:
    fileName = file.partition('.')
    input = open(file, 'r')
    input = input.read()
    # outputFile = open('pages/_PARSED_'+fileName[0]+'.md', 'w')
    outputFile = open(PATH_TO_REPO_HOME +'app/pages/'+'_PARSED_'+fileName[0]+'.md', 'w')
    yamlData = yaml.load(str(input.partition('...')[2].partition('...')[0]))
    if yamlData['folderImg']:
    	folderName = yamlData['folderImg']
    	# rsync local image folders to app/static ?
    	os.system('rsync -rv media/%s  ../static/media/'%(folderName) )
    	outputMd = re.sub(r'(?:!\[(.*)\]\()(.*)(\/)(.*)(?:\))', r'![\1](/static/media/%s/\4)'%(folderName), input )
    outputMd = re.sub(r'(\.\.\.)\n','', outputMd )
    outputFile.write(outputMd)
    outputFile.close()



print('.md files _PARSED_ to pages folder')
