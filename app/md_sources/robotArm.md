...
title: bras robotique
published: 2010-12-24
featured_image:
  - img/photos/bras-robotique1.jpg
quiz: robotArmQuiz
badge: electronique
cat: laser
folderImg : rbarm
type: projet
desc: creation d'un petit bras robotique
...

[TOC]

#Introduction  à ARDUINO <hr> un bras robotique simple

## les enjeux
- découvrir un language de programmation

- les connections électroniques
- les capteurs
- les signaux analogues et digitaux

 programme sur 4 sessions de 2h



## jour 1 : introduction et familiarisation
- introduction pour bemaker
- discussion sur l'openSource openHarware
- premier contact avec l’environement arduino

#### Bemaker
- qui sommes-nous?
- la volonté derrière la création de l'asbl
- les fablabs et leurs atouts (machines  / communauté)

#### OpenSource / OpenHardware

- accessibilités des informations / savoirs
- le champs des possibles de la création à coût réduit avec aide et possiblité de modifier

### Arduino premier contact

#### Arduino IDE et son interface

interface de base de Arduino <abbr title='Integrated development environment' >IDE</abbr> [*](https://en.wikipedia.org/wiki/Integrated_development_environment)

![fuuuuubar](rbarm/ide001_.png)


##### les menus importants

== avant toute chose bien vérifier que la carte *arduino* est la bonne et que le port selectionné correspond dans le menu  `TOOLS`==


 ![](rbarm/ide2_.PNG)


#####  premier code

ouverture d'un exemple simple `blink`

`File > Exemple > 01.basic > blink`

![](rbarm/blink.png)


```c
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

faire un upload sur l' *arduino*  ( <kbd>CTRL + U</kbd> )

>**Exercice**
faire varier le temps de clignotement , 2x plus rapide ? 2 fois plus ***on*** que ***off***


##### introduction à l'idée de variable

- qu'est-ce qu'une variable ? [en savoir plus ](https://fr.wikipedia.org/wiki/Variable_(informatique))


> **Exercice**
créer une variable pour le temps de `delay` afin de pouvoir faire des modifications facilement

*solution*
```c

int delayVal = 1000 ; 				 // creation de la variable

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(delayVal);  				// utilisation de la variable
  digitalWrite(LED_BUILTIN, LOW);
  delay(delayVal);					// utilisation de la variable
}
```

**bonus**
une led 2 fois plus `off` que `on`
```c

int delayVal = 1000 ; 				 // creation de la variable

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(delayVal);  				// utilisation de la variable
 digitalWrite(LED_BUILTIN, LOW);
 delay(delayVal*2);					// utilisation de la variable x 2
}
```



##### Utilisation d'une variable pour gérer une information

faire les branchements sur la carte *arduino* ==UNIQUEMENT avec une carte débranchée==

prendre un `potentiometre` et faire les branchements :

![](rbarm/pot_bb.png)  

le code

```c

const int potPin = A0 ; 				 // Creation de la variable pour le pin input du potentiometre sur le port analogique 0
int potVal  = 0 ;					// initialisation de la vairable pour recevoir la valeur lu sur le port A0
int ledPin = 13;					// Creation de la variable pour le pin output pour une led (sur le port 13 en premier car protege par une resistance)

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
	potVal = analogRead(potPin);
 	digitalWrite(ledPin, HIGH);
	delay(potval);
	digitalWrite(ledPin, LOW);
	delay(potVal);
}
```

**Details**

`const int potPin = A0 ; `

- const : déclaration dune variable Contante (le port A0 ne va pas changer)
- int : création d'une variable de type `integer` , nombre entier
- potPin = A0 : le potentiometre sera donc branche sur le port A0 de l' *arduino*

 `int potVal  = 0 ;`

- int : création d'une variable de type `integer` , nombre entier
- potVal  = 0 : innitialisation de la valeur a `0` la valeur sera mise a jour par la suite

`int ledPin = 13;`

- const : déclaration dune variable Contante (la led reste brancher sur le port 13 tout le temps)
- int : création d'une variable de type `integer` , nombre entier
- ledPin = 13 : la led sera donc branche sur le port 13 de l' *arduino*

`void setup()`
- partie du programme executé UNE SEUL FOIS

`pinMode(ledPin, OUTPUT);`
- déclaration de la Led en tant que OUTPUT

`void loop() `
- partie du programme qui sera repetée en boucle encore et encore


```c
	potVal = analogRead(potPin);		 // lecture de la valeur sur le port A0 de larduino et stockage de la valeur (0-1023) dans la variable prévue
 	digitalWrite(ledPin, HIGH); 		// mise sous tension de la led (on allume)
	delay(potval); 					// faire une pause de la durée récuperée par le capteur (potentiometre)
	digitalWrite(ledPin, LOW);		// mise hors tension de la led (on eteint)
	delay(potVal);					// faire une pause de la durée récuperée par le capteur (potentiometre)

```

*Bravo fin du jour 1!*

![exemple d'un atelier bras robotique](/static/img/photos/bras-robotique1.jpg)


## jour 2 : Premier code pour le bras

**au programme**

- <abbr title='pulse with modulation'> P.W.M </abbr>
- la conversion de donnée (fonction `map()` )
- les différences entre signaux (logic / pwm /analogique)

### mise en place

 ouverture d'un exemple

`File > Exemples > 01.basics > fade`


```

int led = 9;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // set the brightness of pin 9:
  analogWrite(led, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}

```


> **exercice**:
faire en sorte que la lumière ne s’éteigne jamais malgré les variations

explication du pwm
: le pwm est la façon éléctronique de reproduire un signal analogique (par opposition au signal logic `on - off `)



le découpage pour la variation

### experimentation

le but est maintenant de faire un programme de type *dimmer*

check list
- un potentiometre sur le pin A0 de larduino
- une led  sur un port *pwm* de l'arduino (pin avec `~`) et une résistance adaptée

####goal
le potentiometre pour faire varier la luminosité de la led

**code**

todo

- déclaration des variables utiles

- mise en place du setup pour les entrées / sorties
- lecture et applications des variables sur la led et le pwm


**branchement**

 ![](rbarm/pot_bb.png)  


**code**


```c
const int potPin = A0 ;
const int ledPin = 13;

int potVal = 0 ;
int pwmOutput = 0 ;

void setup (){
	pinMode(ledPin, OUTPUT);
}

void loop (){
	pwmOutput = analogRead(potPin);
	pwmOutput = map (pwmOutput , 0,1023,0,255);
	analogWrite(ledPin,pwmOutput);
	delay(100);
}
```


**Explication et théorie**

la function `map()`
:  pourquoi la fonction `map()` ; l'*arduino* ne peut pas faire de signal analogique à proprement parler, il ne peut que être `HIGH` ou `LOW ` , 0 ou 1 , 0V ou 5v pas 2,5v ou 3v pour compenser cette impossibilité les micros contrôleurs peuvent faire du <abbr title='pulse with modulation'> P.W.M </abbr> [en savoir plus >](https://fr.wikipedia.org/wiki/Modulation_de_largeur_d%27impulsion)
c’est à dire allumer ou éteindre rapidement un pin de sortie à différentes fréquences pour que la tension moyenne de sortie corresponde à une valeur d'entre deux.


![](rbarm/pwm.gif#small)
~source Wikipedia~


Pour le micro contrôleur de l' *arduino* il y a 255 valeurs possibles (micro contrôleur *8bits*  [`11111111`]).
le signal analogique de lecture lui en a 1024 [0,1023] la fonction `map()` va nous permettre de faire le calcul et de *remapper* ces valeurs
0 -> 0
512 -> 127
1023 -> 255
c'est un produit en croix qui s'opère dans la fonction ce qui nous permet d'exploiter l’ensemble de la course de notre protentiometre.

~**nb** il sera utile de comprendre la fonction map dans le cadre de la construction du bras robotique pour notre servo moteur `0-1024` -> `0-180`~

### premier mouvement

#### au programme

- première librairie externe
- branchement d'un servo moteur
- contrôle de l'angle avec le potentiometre


**Test du servo moteur**

Ouvrir l'exemple pour le

 <kbd>file</kbd> > <kbd> exemples</kbd> > <kbd>librarie</kbd> >  <kbd>sweep</kbd>

une fois l'exemple ouvert vous devriez avoir ce code

```c
 #include <Servo.h>

Servo myservo;  // create servo object to control a servo
		// a maximum of eight servo objects can be created

int pos = 0;    // variable to store the servo position

void setup()
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}
void loop()
	{
	  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees
	  {                                  // in steps of 1 degree
	    myservo.write(pos);              // tell servo to go to position in variable 'pos'
	    delay(15);                       // waits 15ms for the servo to reach the position
	  }
	  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees
	  {
	    myservo.write(pos);              // tell servo to go to position in variable 'pos'
	    delay(15);                       // waits 15ms for the servo to reach the position
	  }
	}
```

Info
> un servo est un moteur un peu spécial; nous pouvons lui demander un angle précis entre 0 et 180 deg (il ne tourne pas sur lui même mais se déplace sur un demi cercle , pour la plupart d’entre eux )
![servo](rbarm/servomotor.jpg  "interieur servo")
le servo utilise un potentiometre pour connaître sa position


maintenant contrôlons le servo nous même

 <kbd>file</kbd> > <kbd> exemples</kbd> > <kbd>librarie</kbd> >  <kbd>knob</kbd>

 le code suivant est charge

~~~c
 ```
#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int potpin = 0;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin

void setup()
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop()
{
  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 179);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  delay(15);                           // waits for the servo to get there
}
 ```
~~~

#### Explication du code

`#include <Servo.h> `

Chargement de la librairie `servo`
une *librairie * est un dictionnaire de fonctions qui va nous permettre d'augmenter les fonctions d'arduino simplement (sans avoir à écrire bcp de codes la librairie va nous permettre de gérer pleins de choses facilement)

`Servo myservo `

déclaration d'un *objet* 	`Servo` qui va nous permettre par la suite d’interagir avec ce servo via la variable `myservo`

```c
	int potpin = 0;  // analog pin used to connect the potentiometer
	int val;    // variable to read the value from the analog pin
```

déclaration des variables utiles pour la suite de notre code



```c
	void setup()
		{
 	 	myservo.attach(9);  // attaches the servo on pin 9 to the servo object
		}

```

Dans le setup utilisation d’outil de la librairie pour associer le servo au port de sorti numéro `9` de l'arduino pour ainsi plus facilement y avoir acces par la suite







 dans l' `ide` arduino aller dans <kbd>sketch</kbd> > <kbd> include Librairie </kbd> > <kbd> Servo </kbd>




  ![](rbarm/ide003_.png)

nous pouvons maintenant utiliser les fonctions de cette librairie pour faire fonctionner notre servomoteur

```c
 val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 179);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  delay(15);
```

dans l’ordre

1. on récupère la valeur du potentiometre
2. on applique une fonction `map()`  pour transformer notre valeur d'entre de `0-1023` en une valeur pour le servo `0-180`
3. on demande au servo d'aller à la valeur actuelle

nous avons maintenant un servo que nous pouvons contrôler facilement



**GOAL** :

il nous *suffira* maintenant de dupliquer et modifier ce code pour avoir un servo par axes sur notre bras robot



## jour 3 : mesure et dessin

### Goal

- familiarisation avec un logiciel de dessin vectoriel *inkscape* ([site internet ](https://inkscape.org/en/))
- comprendre le dessin entre réel et numérique et créer des fichiers pour la production numérique
- identifier et dessiner les pièces pour le bras robotique

### Inkscape

#### étape 1 : prise de mesures

pour faire des plans il faut des mesures

avec laide de google et d'une recherche simple nous pouvons déjà trouver les infos

vérifier tout de même avec un instrument de mesure que les mesures correspondent à notre équipement

![](rbarm/servodata.png)

avec ceci nous pouvons faire les premiers dessins pour les découpes de placement de servos





#### Dessin dans inkscape



dans inkscape et grâce aux outils cercle*![](rbarm/ink_tools1.png)* et  rectangle  *![](rbarm/ink_tools.png)* créons le dessin suivant

en ajustant les mesures dans la barre supérieure de l'application ![](rbarm/ink_tools3.png)



![](rbarm/ink_firstDraw.png)

avec cela en tête continuez le dessin pour arriver a notre dessin / design pour une partie du bras et recommencer jusqu’à obtention de toutes les pièces

![](rbarm/ink_firstpart.png)



![](rbarm/ink_arm.png)



## jour 4 : code et assemblage



après ces 3 jours de workshop nous avons maintenant tous les éléments utiles à la création de notre bras

#### CODE 	

grâce au code vu en jour 2 nous savons déplacer un servo avec un potetiometre

il suffit donc de dupliquer le code et le répeter pour chaque servo de notre bras

voici le code pour 2 servos à vous de jouer pour les autres




	#include <Servo.h>

	Servo Servo01;  
	Servo Servo02;  

	int potpinServo01 = A0;
	int val01;    

	int potpinServo02 = A1;
	int val02;   

	void setup()
	{
	  Servo01.attach(9);
	  Servo02.attach(10);
	}

	void loop()
	{
	  val01 = analogRead(potpinServo01);
	  val02 = analogRead(potpinServo02);

	  val01 = map(val, 0, 1023, 0, 179);
	  val02 = map(val, 0, 1023, 0, 179);

	  Servo01.write(val01);
	  Servo02.write(val02);

	  delay(15);                           
	}



#### Câblage et Assemblage

![](rbarm/servoWire.png)

faîtes maintenant le câblage de tous les servo selon ce schéma et code

puis assembler les différentes parties du bras robot



BIEN JOUER à bientôt  *![img](rbarm/HTML/img/bm_bw_rbt.png)*
