# eu.bemaker.flask

Rebuild of the bemaker.eu site in Flask.

Main components:
* Flask Security for user logins and user management
* Flask Flatfile to drop .md contents into app/pages resulting in an index of flat files.

Current requirements are as so:

```
git+https://github.com/mattupstate/flask-security
```

### MIGRATION:

pip uninstall Flask-Login Flask-Mail Flask-Principal Flask-WTF isitdangerous passlib

pip install git+https://github.com/mattupstate/flask-security


Mongod startup example for dev purposes:

`mongod --noauth --dbpath /home/colm/git/bemaker/eu.bemaker.flask/data/`

### Translations via flask Babel

— to extract all text and messages:
* `pybabel extract -F babel.cfg -o folder-destination/messages.pot .`

— create translations, the different languages:
* `pybabel init -i messages.pot -d translations -l language`
* `pybabel init -i messages.pot -d translations -l fr`
* `pybabel init -i messages.pot -d translations -l nl`

— to copile:
* `pybabel compile -d parent-translations-folder`
* `pybabel compile -d translations`


### WebFaction
mongoDb application start

https://docs.webfaction.com/software/mongodb.html

`$HOME/webapps/mongoDb/mongodb-linux-x86_64-4.0.0/bin/mongod --auth --dbpath $HOME/webapps/mongodb/data --port 17708`
